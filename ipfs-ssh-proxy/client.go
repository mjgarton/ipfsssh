package main

import (
	"fmt"
	core "github.com/ipfs/go-ipfs/core"
	corenet "github.com/ipfs/go-ipfs/core/corenet"
	config "github.com/ipfs/go-ipfs/repo/config"
	fsrepo "github.com/ipfs/go-ipfs/repo/fsrepo"
	"golang.org/x/net/context"
	peer "gx/ipfs/QmZwZjMVGss5rqYsJVGy18gNbkTJffFyq2x1uJ4e4p3ZAt/go-libp2p-peer"
	"io"
	"os"
	"strings"
	"time"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Please give a peer ID as an argument")
		return
	}
	addr := strings.Replace(os.Args[1], ".ipfs", "", -1)
	//	addr = "QmdB5DpPUsPtsdcbG3qq9i7sQhYuCzNwxBSr8KLoMiqebU"

	fmt.Fprintf(os.Stderr, "going to connect to %v\n", addr)
	target, err := peer.IDB58Decode(addr)
	if err != nil {
		panic(err)
	}

	conf, err := config.Init(os.Stdout, 2048)
	if err != nil {
		panic(err)
	}

	dir := "/tmp/foo"
	err = fsrepo.Init(dir, conf)
	if err != nil {
		panic(err)
	}
	r, err := fsrepo.Open(dir)
	if err != nil {
		panic(err)
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	cfg := &core.BuildCfg{
		Repo:   r,
		Online: true,
	}

	nd, err := core.NewNode(ctx, cfg)

	if err != nil {
		panic(err)
	}

	fmt.Fprintf(os.Stderr, "I am peer %s dialing %s\n", nd.Identity, target)

	var con io.ReadWriteCloser
	for {
		var err error
		con, err = corenet.Dial(nd, target, "/ssh")
		if err == nil {
			break
		}
		fmt.Printf("error %s - will retry in 5s\n", err.Error())
		time.Sleep(5 * time.Second)
	}

	go io.Copy(con, os.Stdin)
	io.Copy(os.Stdout, con)
}
