package main

import (
	"fmt"
	core "github.com/ipfs/go-ipfs/core"
	corenet "github.com/ipfs/go-ipfs/core/corenet"
	fsrepo "github.com/ipfs/go-ipfs/repo/fsrepo"
	"golang.org/x/net/context"
	"io"
	"net"
	"os"
)

func main() {
	// Basic ipfsnode setup
	r, err := fsrepo.Open("~/.ipfs")
	if err != nil {
		panic(err)
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	cfg := &core.BuildCfg{
		Repo:   r,
		Online: true,
	}

	nd, err := core.NewNode(ctx, cfg)

	if err != nil {
		panic(err)
	}

	list, err := corenet.Listen(nd, "/ssh")
	if err != nil {
		panic(err)
	}

	fmt.Fprintf(os.Stderr, "I am peer: %s\n", nd.Identity.Pretty())

	for {
		con, err := list.Accept()
		if err != nil {
			fmt.Println(err)
			return
		}

		go handleConn(con)
	}
}

func handleConn(con io.ReadWriteCloser) {
	println("handling connection")

	defer con.Close()

	println("dialling localhost:22")
	t, err := net.Dial("tcp", "localhost:22")
	if err != nil {
		return
	}

	defer t.Close()

	readDone := make(chan struct{})
	writeDone := make(chan struct{})

	go func() {
		_, err := io.Copy(con, t)
		if err != nil {
			panic(err)
		}
	}()

	go func() {
		_, err := io.Copy(t, con)
		if err != nil {
			panic(err)
		}
	}()

	<-readDone
	<-writeDone

}
